# MagLev-cRIO

Readme for MagLev with CompactRIO

## About

My last project in MEMS was to get a working demo for the MagLev with the CompactRIO 9054 using Real-Time and the FPGA. The cRIO uses a Linux RTOS so it can run deterministically (even using DAQmx) with timing down to the microsecond. FPGA uses hardware timing and can get timing down to the nanosecond accuracy. In this project, I have created LabVIEW projects that will control the MagLev using both a real-time strategy and the FPGA. Note that I did not put a lot of care into optimizing the processes, I just wanted to make a simple example that someone could use as a demo (either when demo-ing the MagLev at Duke/MEMS/Pratt events, or to demo how to communicate between Target/Hosts. 

## How to Run Demos

There are two folders: FPGA and Real-Time. I created two folders/projects so it would be easier to see the two different methods. The FPGA folder contains the LabVIEW files for using the compactRIO FPGA (ns accuracy), while the Real-Time folder is for using the real-time Linux OS with DAQmx driver (us accuracy).

Note that before you run either the FPGA or the Real-Time, you need to set the C Series module to the appropriate mode in NI MAX (find the module in MAX and set it from the drop down menu).

USING REAL-TIME: 

This uses just the Linux processer.

1. Go to the Real-Time folder and Open MagLev with cRIO Real-Time.lvproj
2. Open Host Main.vi and run it. This is your user interface and is running on the Host machine (windows)
3. Open Target Controller.vi. This is running on the cRIO Linux processor (target). It is actually doing the controls. Note I wasn't careful about shared variables initializing property so you have to start this VI second.
4. Demo the maglev!

The two VIs communicate via shared variables. There are a few ways to set this up, and this is a simple demo.

USING THE FPGA:

This has the PID controller loop deployed onto the cRIO FPGA and a separate VI that communicates with that VI's controls/indicators. 

1. Go to FPGA folder and open MagLev with cRIO FPGA.lvproj
2. Open Host Main.vi and run it. This is your user interface. It is running on the cRIO, but could also run on the Windows target. 
3. It may ask you to rebuild the bitfiles for the FPGA, but hopefully that is already done. If not, it takes like 10 minutes to compile
4. Demo the maglev!

## Questions/Issues
contact me (ebs27@duke.edu) if something doesn't work. I hope it's working. Sorry if it isn't.

